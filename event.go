package test

type State struct {
	Posts int
	Active int
	Badge int
}

func state(uid int64) State {
	return State{}
}

type Event struct {
	Predicate func(uid int64) bool
	Function func(uid int64)
}

type Events struct {
	Events []Event
}

func (e *Events) Register(event Event) {
	e.Events = append(e.Events, event)
}

func (e *Events) Execute(uid int64){
	for _, event := range e.Events{
		if event.Predicate(uid) {
			event.Function(uid)
		}
	}
}

func main() {
	e := Events{}
	e.Register(Event{
		Predicate: func(uid int64) bool {
			s := state(uid)
			return s.Posts > 30 && s.Active > 10
		},
		Function: func(uid int64) {
			s := state(uid)
			s.Badge ++
		},
	})
	e.Execute(1)
}
